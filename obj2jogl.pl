#!/usr/bin/env perl
#
# Convert Wavefront models to a Java (JOGL) source file.  The Java source file
# uses classes from the package com.trojanfoe.opengl.
# 
# Andy Duplain, 16-Aug-2010, Initial.
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use File::Find;
use Path::Class;
use Data::Dumper;

my $package = "";
my $vscale = 10.0;
my $nscale = 1.0;
my $size = 0.0;

# Materials.  Array of hashes
my @materials = ();
# Vertices.  Array of triplet floats
my @vertices = ();
# Normals.  Array of triplet floats
my @normals = ();
# Faces. Array of triplet vertex indices followed by triplet normal indices
# followed by material name
my @faces = ();

GetOptions(
    "nscale=i" =>           \$nscale,
    "package=s" =>          \$package,
    "size=i" =>             \$size,
    "vscale=i" =>           \$vscale,
    ) or usage(1);

if (@ARGV != 2)
{
    usage(2);
}

my $indir = $ARGV[0];
my $outdir = $ARGV[1];

opendir(DIR, $indir)
    or die "Failed to open input directory '$indir'";
my @filelist = readdir(DIR);
closedir(DIR);
    
-d $outdir
    or die "Output directory '$outdir' is not a directory";

if (length($package) > 0)
{
    my $subdir = $package;
    # dir() will correct the file separator under Windows automatically...
    $subdir =~ s/\./\//g;
    $outdir = dir($outdir, $subdir);
    if (! -d $outdir)
    {
        print STDERR "Making directory '$outdir'\n";
        mkdir($outdir, 0755)
            or die "Failed to create output directory '$outdir'";
    }
}

foreach (@filelist)
{
    my $filename = dir($indir, $_);
    my ( $name, $path, $extension ) = fileparse($filename, '\..*');
    next if $extension ne ".obj";

    print STDOUT "----- $filename -----\n";    
    readobj($filename);
    resize($size);
    writejava($name);
}

sub readobj
{
    (my $filename) = @_;
    my $currmat = "";
    open(OBJFILE, $filename)
        or die "Failed to open OBJ file '$filename'";

    print STDOUT "Reading OBJ file '$filename'\n";
        
    @vertices = ();
    @normals = ();
    @faces = ();

    while (<OBJFILE>)
    {
        next if m/^#/;
        my @elms = split(/\s/);
        next if $#elms == -1;
        
        if ($elms[0] eq "mtllib")
        {
            readmtl(dir(dirname($filename), $elms[1]));
        }
        elsif ($elms[0] eq "usemtl" || $elms[0] eq "usemat")
        {
            $currmat = $elms[1];
        }
        elsif ($elms[0] eq "v")
        {
            push @vertices, [ $elms[1] * $vscale, $elms[2] * $vscale, $elms[3] * $vscale ];
        }
        elsif ($elms[0] eq "vn")
        {
            push @normals, [ $elms[1] * $nscale, $elms[2] * $nscale, $elms[3] * $nscale ];
        }
        elsif ($elms[0] eq "f")
        {
            my @felms1 = split(/\//, $elms[1]);
            my @felms2 = split(/\//, $elms[2]);
            my @felms3 = split(/\//, $elms[3]);
            if ($#felms1 == 2)
            {
                push @faces, [ $felms1[0] - 1, $felms2[0] - 1, $felms3[0] - 1, $felms1[2] - 1, $felms2[2] - 1, $felms3[2] - 1, $currmat ];
            }
            else
            {
                push @faces, [ $elms[1] - 1, $elms[2] - 1, $elms[3] - 1, $currmat ];
            }
        }
    }
        
    close(OBJFILE);
}

sub readmtl
{
    (my $filename) = @_;
    open(MTLFILE, $filename)
        or die "Failed to open MTL file '$filename'";

    print STDOUT "Reading MTL file '$filename'\n";

    @materials = ();
    my %material = ();
    my $name = "";
    
    while (<MTLFILE>)
    {
        next if m/^#/;
        my @elms = split(/\s/);
        next if $#elms == -1;
        
        if ($elms[0] eq "newmtl")
        {
            $name = $material{'name'};
            if ($name)
            {
                my %matcopy = %material;
                push @materials, \%matcopy;
                %material = ();
            }
            $material{'name'} = $elms[1];
        }
        elsif ($elms[0] eq "Ns")
        {
            $material{'shininess'} = ($elms[1] / 1000.0) * 128.0;
        }
        elsif ($elms[0] eq "Ka")
        {
            $material{'ambient'} = [ $elms[1], $elms[2], $elms[3] ];
        }
        elsif ($elms[0] eq "Kd")
        {
            $material{'diffuse'} = [ $elms[1], $elms[2], $elms[3] ];
        }
        elsif ($elms[0] eq "Ks")
        {
            $material{'specular'} = [ $elms[1], $elms[2], $elms[3] ];
        }
        elsif ($elms[0] eq "d")
        {
            $material{'alpha'} = $elms[1];
        }
        # everthing else is ignored
    }
    $name = $material{'name'};
    if ($name)
    {
        my %matcopy = %material;
        push @materials, \%matcopy;
        %material = ();
    }
        
    close(MTLFILE);
}

sub resize()
{
    # Find the bounding box of the model
    my ($minx, $maxx, $miny, $maxy, $minz, $maxz, $vertex, $i);


    for $i (0 .. $#faces)
    {
        $vertex = $faces[$i][0];
        if (!defined($minx) || $vertices[$vertex][0] < $minx)
        {
            $minx = $vertices[$vertex][0];
        }
        if (!defined($maxx) || $vertices[$vertex][0] > $maxx)
        {
            $maxx = $vertices[$vertex][0];
        }

        $vertex = $faces[$i][1];
        if (!defined($miny) || $vertices[$vertex][1] < $miny)
        {
            $miny = $vertices[$vertex][1];
        }
        if (!defined($maxy) || $vertices[$vertex][1] > $maxy)
        {
            $maxy = $vertices[$vertex][1];
        }

        $vertex = $faces[$i][2];
        if (!defined($minz) || $vertices[$vertex][2] < $minz)
        {
            $minz = $vertices[$vertex][2];
        }
        if (!defined($maxz) || $vertices[$vertex][2] > $maxz)
        {
            $maxz = $vertices[$vertex][2];
        }
    }

    my ($sizex, $sizey, $sizez, $largest, $scale);
    $sizex = $maxx - $minx;
    $sizey = $maxy - $miny;
    $sizez = $maxz - $minz;

    if ($sizex > $sizey)
    {
        $largest = $sizex;
    }
    else
    {
        $largest = $sizey;
    }
    if ($sizez > $largest)
    {
        $largest = $sizez;
    }

    if ($size > 0.0)
    {
        $scale = $size / $largest;

        my $sizex_scaled = $sizex * $scale;
        my $sizey_scaled = $sizey * $scale;
        my $sizez_scaled = $sizez * $scale;

        printf STDOUT "Bounding box [%.3f, %.3f, %.3f] * %.3f = [%.3f, %.3f, %.3f]\n",
            $sizex, $sizey, $sizez, $scale, $sizex_scaled, $sizey_scaled, $sizez_scaled;

        for $i (0 .. $#vertices)
        {
            $vertices[$i][0] *= $scale;
            $vertices[$i][1] *= $scale;
            $vertices[$i][2] *= $scale;
        }
    }
}

sub writejava
{
    my ($name) = @_;
    my $classname = ucfirst($name);
    my $filename = file($outdir, $classname . ".java");
    open (OUTFILE, "> $filename")
        or die "Failed to open Java file '$filename'";

    my $hasnormals = $#normals > -1;

    print STDOUT "Writing Java file '$filename'\n";

    if (length($package) > 0)
    {
        print OUTFILE "package $package;\n\n";
    }

    print OUTFILE "import com.trojanfoe.opengl.Vector3f;\n";
    print OUTFILE "import com.trojanfoe.opengl.Face;\n";
    print OUTFILE "import com.trojanfoe.opengl.Material;\n";
    print OUTFILE "import com.trojanfoe.opengl.Model;\n";
    print OUTFILE "\n";
    print OUTFILE "public class $classname extends Model\n";
    print OUTFILE "{\n";    
    print OUTFILE "    public $classname()\n";
    print OUTFILE "    {\n";
    print OUTFILE "        super(\"$classname\", null, null, null);\n";
    print OUTFILE "        createMaterials();\n";
    print OUTFILE "        createVertices();\n";
    print OUTFILE "        createFaces();\n";
    if ($#normals > -1)
    {
        print OUTFILE "        createNormals();\n";
    }
    else
    {
        print OUTFILE "        calcNormals();\n";
    }
    print OUTFILE "    }\n\n";
    print OUTFILE "    protected void createMaterials()\n";
    print OUTFILE "    {\n";
    print OUTFILE "        materials = new Material[]\n";
    print OUTFILE "        {\n";
    my ($i, $a, $b, $c);
    for $i (0 .. $#materials)
    {
        print OUTFILE "            new Material(";
        print OUTFILE "\"" . $materials[$i]{'name'} . "\", ";
        $a = $materials[$i]{'ambient'}[0];
        $b = $materials[$i]{'ambient'}[1];
        $c = $materials[$i]{'ambient'}[2];
        print OUTFILE "new Vector3f(${a}f, ${b}f, ${c}f), ";
        $a = $materials[$i]{'diffuse'}[0];
        $b = $materials[$i]{'diffuse'}[1];
        $c = $materials[$i]{'diffuse'}[2];
        print OUTFILE "new Vector3f(${a}f, ${b}f, ${c}f), ";
        $a = $materials[$i]{'specular'}[0];
        $b = $materials[$i]{'specular'}[1];
        $c = $materials[$i]{'specular'}[2];
        print OUTFILE "new Vector3f(${a}f, ${b}f, ${c}f), ";
        $a = $materials[$i]{'alpha'};
        $b = $materials[$i]{'shininess'};
        print OUTFILE "${b}f, ${a}f)";
        if ($i < $#materials)
        {
            print OUTFILE ",";
        }
        print OUTFILE "\n";
    }
    print OUTFILE "        };\n";
    print OUTFILE "    }\n\n";
    print OUTFILE "    protected void createVertices()\n";
    print OUTFILE "    {\n";
    
    print OUTFILE "        vertices = new Vector3f[]\n";
    print OUTFILE "        {\n";
    for $i (0 .. $#vertices)
    {
        print OUTFILE "            new Vector3f(" . $vertices[$i][0] . "f, " . $vertices[$i][1] . "f, " . $vertices[$i][2] . "f)";
        if ($i < $#vertices)
        {
            print OUTFILE ",";
        }
        print OUTFILE "\n";
    }
    print OUTFILE "        };\n";
    print OUTFILE "    }\n\n";

    if ($#normals > -1)
    {
        print OUTFILE "    protected void createNormals()\n";
        print OUTFILE "    {\n";
        print OUTFILE "        normals = new Vector3f[]\n";
        print OUTFILE "        {\n";
        for $i (0 .. $#normals)
        {
            print OUTFILE "            new Vector3f(" . $normals[$i][0] . "f, " . $normals[$i][1] . "f, " . $normals[$i][2] . "f)";
            if ($i < $#normals)
            {
                print OUTFILE ",";
            }
            print OUTFILE "\n";
        }
        print OUTFILE "        };\n";
        print OUTFILE "    }\n\n";
    }

    print OUTFILE "    protected void createFaces()\n";
    print OUTFILE "    {\n";
    print OUTFILE "        faces = new Face[]\n";
    print OUTFILE "        {\n";
    for $i (0 .. $#faces)
    {
        if ($hasnormals)
        {
            print OUTFILE "            new Face(" . $faces[$i][0] . ", " . $faces[$i][1] . ", " . $faces[$i][2] . ", " .
                      $faces[$i][3] . ", " . $faces[$i][4] . ", " . $faces[$i][5] . ", materials[" . getmatindex($faces[$i][6]) . "])";
        }
        else
        {
            print OUTFILE "            new Face(" . $faces[$i][0] . ", " . $faces[$i][1] . ", " . $faces[$i][2] . ", materials[" . getmatindex($faces[$i][3]) . "])";
        }
        if ($i < $#faces)
        {
            print OUTFILE ",";
        }
        print OUTFILE "\n";
    }
    print OUTFILE "        };\n";
    print OUTFILE "    }\n";
    print OUTFILE "}\n";

    close(OUTFILE);
}

sub getmatindex
{
    my ($name) = @_;
    for my $i (0 .. $#materials)
    {
        if ($materials[$i]{'name'} eq $name)
        {
            return $i;
        }
    }
    return -1;
}

sub usage
{
    (my $exitcode) = @_;
    print STDERR "Usage: obj2jogl.pl [options] indir outdir\n";
    print STDERR "Options:\n";
    print STDERR "  --nscale scale             Amount to scale normals [1.0].\n";
    print STDERR "  --package package          Set the package name of the Java output files.\n";
    print STDERR "  --size value               Size to make model.\n";
    print STDERR "  --vscale scale             Amount to scale vertices [10.0].\n";
    print STDERR "\n";
    print STDERR "'indir' is scanned for files with the extension '.obj' and all output is\n";
    print STDERR "written into the 'outdir' directory, obeying the normal Java class naming\n";
    print STDERR "directory structure convention (i.e. if package is set to 'a.b.c' and\n";
    print STDERR "'outdir' is set to '../project/src', then files are written to\n";
    print STDERR "'../project/src/a/b/c/').\n";
    exit($exitcode);
}

